<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events\Amqp;

use devyk\amqp\components\AmqpMessage;
use devyk\amqp\factories\AmqpMessageFactory;

class Amqp implements AmqpInterface
{
    /** @var AmqpClientFactory */
    protected $amqpClientFactory;

    /** @var AmqpMessageFactory */
    protected $amqpMessageFactory;

    /** @var array */
    protected $config;

    /**
     * @param AmqpClientFactory $amqpClientFactory
     * @param AmqpMessageFactory $amqpMessageFactory
     * @param array $config
     */
    public function __construct(
        AmqpClientFactory $amqpClientFactory,
        AmqpMessageFactory $amqpMessageFactory,
        array $config
    ) {
        $this->amqpClientFactory = $amqpClientFactory;
        $this->amqpMessageFactory = $amqpMessageFactory;
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function pushToQueue(array $data) : void
    {
        $this->amqpClientFactory->create()->getChannel()->basic_publish(
            $this->createAmqpMessage($data),
            $this->config['defaultExchange'],
            $this->config['defaultRoutingKey']
        );
    }

    /**
     * @inheritdoc
     */
    public function pushToDelayedQueue(array $data, int $delay) : void
    {
        $this->amqpClientFactory->create()->getChannel()->basic_publish(
            $this->createAmqpMessage($data)->setDelay($delay),
            $this->config['delayedExchange'],
            $this->config['delayedRoutingKey']
        );
    }

    /**
     * Creates AMQP message
     * @param array $data
     * @return AmqpMessage
     */
    protected function createAmqpMessage(array $data) : AmqpMessage
    {
        return $this->amqpMessageFactory->create([
            'body' => $data,
            'properties' => ['delivery_mode' => AmqpMessage::DELIVERY_MODE_PERSISTENT]
        ]);
    }
}
